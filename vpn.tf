## Create Azure Virtual WAN since users will authenticate with AD
resource "azurerm_virtual_wan" "vwan" {
  name                = var.vwan_name
  resource_group_name = var.resource_group_name
  location            = var.location
  type                = var.vwan_type
  tags                = var.tags
}

resource "azurerm_virtual_hub" "vhub" {
  name                = var.vhub_name
  resource_group_name = var.resource_group_name
  location            = var.location
  virtual_wan_id      = azurerm_virtual_wan.vwan.id
  address_prefix      = var.vhub_address_prefix
  tags                = var.tags
}

resource "azurerm_vpn_server_configuration" "vhub_vpn_server" {
  name                     = var.vhub_vpn_server_name
  resource_group_name      = var.resource_group_name
  location                 = var.location
  vpn_authentication_types = ["AAD"]

  azure_active_directory_authentication {
    audience = var.vhub_vpn_config.azure_vpn_app_id
    issuer   = "https://sts.windows.net/${var.vhub_vpn_config.directory_id}/"
    tenant   = "https://login.microsoftonline.com/${var.vhub_vpn_config.directory_id}"
  }
  depends_on = [azurerm_virtual_hub.vhub]
  tags       = var.tags
}

resource "azurerm_point_to_site_vpn_gateway" "vhub_p2s_gateway" {
  name                        = var.vhub_p2s_gateway.name
  resource_group_name         = var.resource_group_name
  location                    = var.location
  virtual_hub_id              = azurerm_virtual_hub.vhub.id
  vpn_server_configuration_id = azurerm_vpn_server_configuration.vhub_vpn_server.id
  scale_unit                  = 1

  connection_configuration {
    name = var.vhub_p2s_gateway.connection_name
    vpn_client_address_pool {
      address_prefixes = [var.vhub_p2s_gateway.address_prefixes]
    }
  }
  depends_on = [azurerm_virtual_hub.vhub, azurerm_vpn_server_configuration.vhub_vpn_server]
  tags       = var.tags
}

resource "azurerm_virtual_hub_connection" "vnet_connection" {
  name                      = var.vhub_connection_name
  virtual_hub_id            = azurerm_virtual_hub.vhub.id
  remote_virtual_network_id = var.vnet_id
  depends_on                = [azurerm_virtual_hub.vhub, azurerm_point_to_site_vpn_gateway.vhub_p2s_gateway]
}