
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Azure VPN

Terraform module which create Azure VPN related resources with Active Directory authentication. The VPN enables end user to connect with the defined subnet.

## Prerequisites

- An Azure Active Directory user that have a Global administrator role
- Installed and consent granted **Azure VPN** entreprise app. See [here](https://docs.microsoft.com/en-us/azure/virtual-wan/openvpn-azure-ad-tenant#enable-authentication) for further informations.

## Module usage

```hcl
provider "azuread" {
  tenant_id = var.tenant_id
}

provider "azurerm" {
  client_id = var.client_id
  features {}
}

module "azure_network" {
  source = "git::https://gitlab.com/keltiotechnology/terraform/modules/azure/azure-base-networks.git?ref=main"

  ## Tags ##
  tags = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Virtual Network Variables ##
  vnet_name    = var.vnet_name
  network_cidr = var.network_cidr

  ## Public Subnet Variables ##
  public_subnets = var.public_subnets

  ## Private Subnet Variables ##
  private_subnets = var.private_subnets

  ## NAT Gateway Variables ##
  public_subnet_gateway_ip = var.public_subnet_gateway_ip
  public_subnet_gateway    = var.public_subnet_gateway
}

module "azure_vpn" {
  source = "../.."

  ## Rerource Group ##
  location            = var.location
  resource_group_name = var.resource_group_name

  ## Virtual WAN ##
  vwan_name = var.vwan_name
  vwan_type = var.vwan_type

  ## Virtual HUB ##
  vhub_name            = var.vhub_name
  vhub_address_prefix  = var.vhub_address_prefix
  vhub_vpn_server_name = var.vhub_vpn_server_name
  vhub_vpn_config      = var.vhub_vpn_config
  vhub_p2s_gateway     = var.vhub_p2s_gateway
  vhub_connection_name = var.vhub_connection_name

  ## Virtual Network ##
  vnet_id = module.azure_network.virtual_network.id

  ## Tags ##
  tags = var.tags

  depends_on = [module.azure_network]
}
```
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_azuread"></a> [azuread](#requirement\_azuread) | >=2.5.0 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >=2.79.1 |

## Resources

| Name | Type |
|------|------|
| [azurerm_point_to_site_vpn_gateway.vhub_p2s_gateway](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/point_to_site_vpn_gateway) | resource |
| [azurerm_virtual_hub.vhub](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_hub) | resource |
| [azurerm_virtual_hub_connection.vnet_connection](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_hub_connection) | resource |
| [azurerm_virtual_wan.vwan](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_wan) | resource |
| [azurerm_vpn_server_configuration.vhub_vpn_server](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/vpn_server_configuration) | resource |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | 2.80.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_location"></a> [location](#input\_location) | Azure region where the resource group will be created | `string` | `"france central"` | no |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | Name of the resource group | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Resources Tags | `map(any)` | n/a | yes |
| <a name="input_vhub_address_prefix"></a> [vhub\_address\_prefix](#input\_vhub\_address\_prefix) | Hub IP address prefix | `string` | `"172.16.0.0/16"` | no |
| <a name="input_vhub_connection_name"></a> [vhub\_connection\_name](#input\_vhub\_connection\_name) | Virtual hub connection name | `string` | `"vm_vhub_connection"` | no |
| <a name="input_vhub_name"></a> [vhub\_name](#input\_vhub\_name) | Hub name for the virtual WAN | `string` | `"virtual-wan-hub"` | no |
| <a name="input_vhub_p2s_gateway"></a> [vhub\_p2s\_gateway](#input\_vhub\_p2s\_gateway) | Virtual hub point to site VPN gateway properties | `map(any)` | <pre>{<br>  "address_prefixes": "10.0.10.0/24",<br>  "connection_name": "vhub_p2s_gateway_connection",<br>  "name": "vhub_p2s_gateway"<br>}</pre> | no |
| <a name="input_vhub_vpn_config"></a> [vhub\_vpn\_config](#input\_vhub\_vpn\_config) | Azure VPN Configurations | `map(any)` | <pre>{<br>  "azure_vpn_app_id": "",<br>  "directory_id": ""<br>}</pre> | no |
| <a name="input_vhub_vpn_server_name"></a> [vhub\_vpn\_server\_name](#input\_vhub\_vpn\_server\_name) | Name for the virtual hub vpn server | `string` | n/a | yes |
| <a name="input_vnet_id"></a> [vnet\_id](#input\_vnet\_id) | Virtual Network ID to access from the VPN | `string` | n/a | yes |
| <a name="input_vwan_name"></a> [vwan\_name](#input\_vwan\_name) | Azure virtual WAN name | `string` | n/a | yes |
| <a name="input_vwan_type"></a> [vwan\_type](#input\_vwan\_type) | Azure virtual WAN type | `string` | `"Standard"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_point2site_id"></a> [point2site\_id](#output\_point2site\_id) | Azure point to site gateway ID |
| <a name="output_vhub_id"></a> [vhub\_id](#output\_vhub\_id) | Azure virtual hub ID |
| <a name="output_vwan_id"></a> [vwan\_id](#output\_vwan\_id) | Azure virtual WAN ID |  
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->