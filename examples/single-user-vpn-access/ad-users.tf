resource "azuread_user" "ad_users" {
  for_each = var.ad_users

  user_principal_name   = each.value.user_principal_name
  password              = each.value.password
  force_password_change = each.value.force_password_change
  display_name          = each.value.display_name
  department            = each.value.department
  job_title             = each.value.job_title
}
