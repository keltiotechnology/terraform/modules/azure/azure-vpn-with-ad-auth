## Providers ##
variable "tenant_id" {
  type        = string
  description = "Tenant ID for the Azure subscription"
}

## Tags ##
variable "tags" {
  type        = map(any)
  description = "Resources Tags"
}

variable "client_id" {
  type        = string
  description = "Client ID for the Azure subscription"
}

## Resource Group Variables ##
variable "location" {
  type        = string
  description = "Azure region where the resource group will be created"
  default     = "france central"
}
variable "resource_group_name" {
  type        = string
  description = "Name of the resource group"
}

## Azure Active Directory ##
variable "ad_users" {
  type        = map(any)
  description = "Azure Application AD name"
}

## Virtual WAN ##
variable "vwan_name" {
  type        = string
  description = "Azure virtual WAN name"
}

variable "vwan_type" {
  type        = string
  description = "Azure virtual WAN type"
  default     = "Standard"
}

## Virtual WAN ##
variable "vhub_name" {
  type        = string
  description = "Hub name for the virtual WAN"
  default     = "virtual-wan-hub"
}

variable "vhub_address_prefix" {
  type        = string
  description = "Hub IP address prefix"
  default     = "172.16.0.0/16"
}

variable "vhub_vpn_server_name" {
  type        = string
  description = "Name for the virtual hub vpn server"
}

variable "vhub_vpn_config" {
  type        = map(any)
  description = "Azure VPN Configurations"
  default = {
    azure_vpn_app_id = "your-azure-vpn-entreprise-app-id"
    directory_id     = "your-tenant-id"
  }
}

variable "vhub_p2s_gateway" {
  type        = map(any)
  description = "Virtual hub point to site VPN gateway properties"
}

variable "vhub_connection_name" {
  type        = string
  description = "Virtual hub connection name"
}