## VM Configs ##
variable "vm" {
  type        = any
  description = "Virtual machine properties"
}
variable "vm_nic_name" {
  type        = string
  description = "Network interaface name for the virtual machine"
  default     = "vm_nic"
}

variable "vm_ip_forward" {
  type        = bool
  description = "IP forwarding rule switch on the VM NIC"
  default     = true
}

variable "vm_ip_allocation" {
  type        = string
  description = "IP allocation mode for the VM"
  default     = "Dynamic"
}