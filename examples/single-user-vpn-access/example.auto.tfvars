## Providers Variables - Replace with yours ##
client_id = "b15e3114-37af-4463-8659-786dc5cc0bff"
tenant_id = "1f9c97e0-b9b8-4097-9574-6b32b8e5190e"

## Tags ##
tags = {
  Environment = "testing"
}

## Resource Group Variables ##
resource_group_name = "example_azure_resource_group"
location            = "francecentral"

## Azure Active Directory ##
ad_users = {
  vpn_user = {
    user_principal_name   = "vpnuserexample@supporthairuntechnology.onmicrosoft.com"
    password              = "FooBaR2021!!"
    force_password_change = true
    display_name          = "Azure VPN User"
    department            = "Infrastructure"
    job_title             = "DevOps Engineer"
  }
}

## Virtual WAN ##
vwan_name = "vm_vwan"
vwan_type = "Standard"

## Virtual HUB ##
vhub_name            = "vm_vhub"
vhub_connection_name = "vm_vhub_connection"
vhub_address_prefix  = "172.10.0.0/16"
vhub_vpn_server_name = "vm_vpn"
vhub_vpn_config = {
  # Your Azure VPN Entreprise app ID
  azure_vpn_app_id = "41b23e61-6c1e-4545-b367-cd054e0ed4b4"
  # Your tenant/directory ID
  directory_id = "1f9c97e0-b9b8-4097-9574-6b32b8e5190e"
}
vhub_p2s_gateway = {
  name             = "vhub_p2s_gateway"
  connection_name  = "vhub_p2s_gateway_connection"
  address_prefixes = "192.168.10.0/24"
}

## Virtual Machine ##
vm = {
  name                  = "basicvm"
  size                  = "Standard_B2s"
  username              = "foo"
  admin_sshkey_pub_path = "~/.ssh/id_rsa.pub"
  disk = {
    name              = "vmosdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  image = {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18_04-lts-gen2"
    version   = "latest"
  }
}