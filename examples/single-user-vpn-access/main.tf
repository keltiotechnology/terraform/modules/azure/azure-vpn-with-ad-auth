provider "azuread" {
  tenant_id = var.tenant_id
}

provider "azurerm" {
  client_id = var.client_id
  features {}
}

module "azure_network" {
  source = "git::https://gitlab.com/keltiotechnology/terraform/modules/azure/azure-base-networks.git?ref=main"

  ## Tags ##
  tags = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Virtual Network Variables ##
  vnet_name    = var.vnet_name
  network_cidr = var.network_cidr

  ## Public Subnet Variables ##
  public_subnets = var.public_subnets

  ## Private Subnet Variables ##
  private_subnets = var.private_subnets

  ## NAT Gateway Variables ##
  public_subnet_gateway_ip = var.public_subnet_gateway_ip
  public_subnet_gateway    = var.public_subnet_gateway
}

module "azure_vpn" {
  source = "../.."

  ## Rerource Group ##
  location            = var.location
  resource_group_name = var.resource_group_name

  ## Virtual WAN ##
  vwan_name = var.vwan_name
  vwan_type = var.vwan_type

  ## Virtual HUB ##
  vhub_name            = var.vhub_name
  vhub_address_prefix  = var.vhub_address_prefix
  vhub_vpn_server_name = var.vhub_vpn_server_name
  vhub_vpn_config      = var.vhub_vpn_config
  vhub_p2s_gateway     = var.vhub_p2s_gateway
  vhub_connection_name = var.vhub_connection_name

  ## Virtual Network ##
  vnet_id = module.azure_network.virtual_network.id

  ## Tags ##
  tags = var.tags

  depends_on = [module.azure_network]
}