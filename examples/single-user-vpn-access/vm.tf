## Virtual Machine as an example ##
resource "azurerm_network_interface" "vm_nic" {
  name                 = var.vm_nic_name
  location             = var.location
  resource_group_name  = var.resource_group_name
  enable_ip_forwarding = var.vm_ip_forward

  ip_configuration {
    name                          = "${var.vm_nic_name}_default"
    subnet_id                     = module.azure_network.private_subnets["private_subnet_compute"].id
    private_ip_address_allocation = var.vm_ip_allocation
  }
  tags = var.tags
}

resource "azurerm_linux_virtual_machine" "vm" {
  name                  = var.vm.name
  location              = var.location
  resource_group_name   = var.resource_group_name
  size                  = var.vm.size
  admin_username        = var.vm.username
  network_interface_ids = [azurerm_network_interface.vm_nic.id]

  admin_ssh_key {
    username   = var.vm.username
    public_key = file(var.vm.admin_sshkey_pub_path)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = var.vm.image.publisher
    offer     = var.vm.image.offer
    sku       = var.vm.image.sku
    version   = var.vm.image.version
  }
  tags = var.tags
}