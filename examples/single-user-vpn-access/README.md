# Notes

This example will create an Azure VPN and an active directory user that can authenticate with the Azure VPN

- After successfully applying this example, download User VPN Profile from the Azure portal virtual WAN menu by following this [guide](https://docs.microsoft.com/en-us/azure/virtual-wan/virtual-wan-point-to-site-azure-ad#download-user-vpn-profile)
- Then, configure the Azure VPN Client by following this [guide](https://docs.microsoft.com/en-us/azure/virtual-wan/virtual-wan-point-to-site-azure-ad#configure-user-vpn-clients)