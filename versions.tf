terraform {
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = ">=2.5.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=2.79.1"
    }
  }
}