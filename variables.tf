## Tags ##
variable "tags" {
  type        = map(any)
  description = "Resources Tags"
}

## Resource Group Variables ##
variable "location" {
  type        = string
  description = "Azure region where the resource group will be created"
  default     = "france central"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the resource group"
}

## Virtual WAN ##
variable "vwan_name" {
  type        = string
  description = "Azure virtual WAN name"
}

variable "vwan_type" {
  type        = string
  description = "Azure virtual WAN type"
  default     = "Standard"
}

## Virtual HUB ##
variable "vhub_name" {
  type        = string
  description = "Hub name for the virtual WAN"
  default     = "virtual-wan-hub"
}

variable "vhub_address_prefix" {
  type        = string
  description = "Hub IP address prefix"
  default     = "172.16.0.0/16"
}

variable "vhub_vpn_server_name" {
  type        = string
  description = "Name for the virtual hub vpn server"
}

variable "vhub_vpn_config" {
  type        = map(any)
  description = "Azure VPN Configurations"
  default = {
    azure_vpn_app_id = ""
    directory_id     = ""
  }
}

variable "vhub_p2s_gateway" {
  type        = map(any)
  description = "Virtual hub point to site VPN gateway properties"
  default = {
    name             = "vhub_p2s_gateway"
    connection_name  = "vhub_p2s_gateway_connection"
    address_prefixes = "10.0.10.0/24"
  }
}

variable "vhub_connection_name" {
  type        = string
  description = "Virtual hub connection name"
  default     = "vm_vhub_connection"
}

## Virtual Network Inputs ##
variable "vnet_id" {
  type        = string
  description = "Virtual Network ID to access from the VPN"
}