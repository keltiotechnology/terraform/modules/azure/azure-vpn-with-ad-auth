output "vwan_id" {
  description = "Azure virtual WAN ID"
  value       = azurerm_virtual_wan.vwan.id
}

output "vhub_id" {
  description = "Azure virtual hub ID"
  value       = azurerm_virtual_hub.vhub.id
}

output "point2site_id" {
  description = "Azure point to site gateway ID"
  value       = azurerm_point_to_site_vpn_gateway.vhub_p2s_gateway.id
}